'use strict';
CKEDITOR.dialog.add('imageDGEDialog', function (editor) {
    return {
        title: 'Image',
        id: 'imageDGEDialog',
        minWidth: 500,
        minHeight: 200,
        resizable: true,
        contents: [
            {
                id: 'informations',
                label: 'Informations sur l\'image',
                elements:
                    [
                        {
                            type: 'hbox',
                            children: [
                                {
                                    type: 'text',
                                    label: 'URL',
                                    id: 'url',
                                    default: '',
                                    className: 'disabled',
                                },
                                {
                                    type: 'button',
                                    id: 'browse',
                                    label: 'Choisir une image',
                                    filebrowser: {
                                        action: 'Browse',
                                        url: CKEDITOR.imce.url('sendto=imageHandler'),
                                        target: 'informations:url',
                                    },
                                    style: 'margin-top:25px; margin-left:25px',
                                },
                            ]
                        },
                        {
                            type: 'hbox',
                            children: [
                                {
                                    type: 'text',
                                    label: 'Texte de remplacement (alt)',
                                    id: 'alt',
                                    default: '',
                                    onKeyUp: function() {
                                        changePreview(this.id);
                                    },
                                },
                                {
                                    type: 'text',
                                    label: 'Titre (title)',
                                    id: 'title',
                                    default: '',
                                    onKeyUp: function() {
                                        changePreview(this.id);
                                    },
                                },
                                {
                                    type: 'text',
                                    label: 'Copyright',
                                    id: 'copyright',
                                    default: '',
                                    onKeyUp: function() {
                                        changePreview(this.id);
                                    },
                                },
                            ]
                        },
                        {
                            type: 'hbox',
                            children: [
                                {
                                    type: 'text',
                                    label: 'Lien (veuillez mettre un lien sous la forme "https://exemple.com")',
                                    id: 'lien',
                                    default: '',
                                    onKeyUp: function() {
                                        changePreview(this.id);
                                    },
                                },
                            ]
                        },
                        {
                            type: 'hbox',
                            children: [
                                {
                                    type: 'vbox',
                                    children: [
                                        {
                                            type: 'text',
                                            label: 'Largeur',
                                            id: 'width',
                                            default: '',
                                            onKeyUp: function() {
                                                changePreview(this.id);
                                            },
                                        },
                                        {
                                            type: 'text',
                                            label: 'Hauteur',
                                            id: 'height',
                                            default: '',
                                            onKeyUp: function() {
                                                changePreview(this.id);
                                            },
                                        },
                                        {
                                            type: 'text',
                                            label: 'Bordure',
                                            id: 'border',
                                            default: '',
                                            onKeyUp: function() {
                                                changePreview(this.id);
                                            },
                                        },
                                        {
                                            type: 'text',
                                            label: 'Marge du haut',
                                            id: 'margin-top',
                                            default: '',
                                            onKeyUp: function() {
                                                changePreview(this.id);
                                            },
                                        },
                                        {
                                            type: 'text',
                                            label: 'Marge du bas',
                                            id: 'margin-bottom',
                                            default: '',
                                            onKeyUp: function() {
                                                changePreview(this.id);
                                            },
                                        },
                                        {
                                            type: 'text',
                                            label: 'Marge de droite',
                                            id: 'margin-right',
                                            default: '',
                                            onKeyUp: function() {
                                                changePreview(this.id);
                                            },
                                        },
                                        {
                                            type: 'text',
                                            label: 'Marge de gauche',
                                            id: 'margin-left',
                                            default: '',
                                            onKeyUp: function() {
                                                changePreview(this.id);
                                            },
                                        },
                                        {
                                            type: 'select',
                                            label: 'Alignement',
                                            id: 'float',
                                            items: [ [ 'Aucun' ], [ 'Gauche' ], [ 'Droite' ] ],
                                            'default': 'Aucun',
                                            onChange: function() {
                                                changePreview(this.id);
                                            },
                                        },
                                    ]
                                },
                                {
                                    type: 'vbox',
                                    children: [
                                        {
                                            type: 'html',
                                            label: 'Preview',
                                            id: 'preview',
                                            html: '',
                                        },
                                    ]
                                },
                            ]
                        },
                    ]
            }
        ],

        onOk : function() {
            let dialog = CKEDITOR.dialog.getCurrent();
            let url = dialog.getContentElement('informations', 'url').getValue();
            if (url !== '') {
                let element = dialog.getContentElement('informations', 'preview').getElement();
                let html = element.$.innerHTML;
                let previewsElement = document.createElement('div');
                previewsElement.innerHTML = html;
                let img = previewsElement.getElementsByTagName('img')[0];
                // On retire les attributs CKEditor, qui causent des problèmes lors du remplacement d'une image
                img.removeAttribute("data-widget");
                img.removeAttribute("data-cke-saved-src");
                img.removeAttribute("data-cke-widget-keep-attr");
                img.removeAttribute("data-cke-widget-upcasted");
                img.removeAttribute("data-cke-widget-data");
                if (dialog.getContentElement('informations', 'lien').getValue().length !== 0) {
                    let a = document.createElement('a');
                    a.href = dialog.getContentElement('informations', 'lien').getValue();
                    a.target = '_blank';
                    a.appendChild(img);
                }
                let float = '';
                if (img.classList.contains('img-float-left')) float = ' img-float-left';
                if (img.classList.contains('img-float-right')) float = ' img-float-right';
                let insert;
                let valueCopyright = dialog.getContentElement('informations', 'copyright').getValue();
                let valueLien = dialog.getContentElement('informations', 'lien').getValue();
                if (valueCopyright !== '') {
                    if (valueLien !== '') {
                        insert =  CKEDITOR.dom.element.createFromHtml('<span class="span-img' + float + '"><a href="'+ valueLien +'" target="_blank" class="img-dge">'+ img.outerHTML + '</a><span class="copyright">' + valueCopyright + '</span></span>');
                    } else {
                        insert =  CKEDITOR.dom.element.createFromHtml('<span class="span-img' + float + '">'+ img.outerHTML + '<span class="copyright">' + valueCopyright + '</span></span>');
                    }
                } else {
                    if (valueLien !== '') {
                        insert = CKEDITOR.dom.element.createFromHtml('<span class="span-img' + float + '"><a href="'+ valueLien +'" target="_blank" class="img-dge">'+ img.outerHTML + '</a></span>');
                    } else {
                        insert = CKEDITOR.dom.element.createFromHtml('<span class="span-img' + float + '">'+ img.outerHTML + '</span>');
                    }
                }
                editor.insertElement(insert);

                // https://github.com/ckeditor/ckeditor-dev/issues/1180
                setTimeout(function(){
                    let div = document.createElement('div');
                    div.innerHTML = editor.getData();
                    let spans = div.getElementsByClassName('span-img');
                    for (let i = 0; i < spans.length; i++) {
                        if (spans[i].parentElement.parentElement.classList.contains('span-img')) {
                            spans[i].parentElement.replaceWith(spans[i]);
                        }
                    }
                    editor.setData(div.innerHTML);
                }, 200);

                setTimeout(function(){
                    let div = document.createElement('div');
                    div.innerHTML = editor.getData();
                    let spans = div.getElementsByClassName('span-img');
                    for (let i = 0; i < spans.length; i++) {
                        if (spans[i].parentElement.classList.contains('span-img')) {
                            spans[i].parentElement.replaceWith(spans[i]);
                        }
                    }
                    editor.setData(div.innerHTML);
                }, 500);
            }
        },

        onShow: function() {
            let inputUrl = document.getElementsByClassName('disabled')[0].children[1].children[0].children[0];
            inputUrl.disabled = true;
            inputUrl.readOnly = true;
            inputUrl.style.backgroundColor = '#EBEBE4';

            document.querySelector('[title="Fermer"]').style.display = 'none';
            document.querySelector('[title="Annuler"]').style.display = 'none';

            let selection = editor.getSelection();
            let element = selection.getStartElement();
            let parent = element.$.parentElement;
            if (parent.classList.contains('span-img')) {
                setPreview(true, parent)
            } else if (parent.classList.contains('img-dge')) {
                setPreview(true, parent);
            } else {
                setPreview(false)
            }
        },
    }
});

function imageHandler(File, imceWin) {
    let dialog = CKEDITOR.dialog.getCurrent();
    dialog.getContentElement('informations', 'url').setValue(File.getUrl());
    dialog.getContentElement('informations', 'width').setValue(File.width);
    dialog.getContentElement('informations', 'height').setValue(File.height);

    window.imageWidht = File.width;
    window.imageHeight = File.height;

    /*dialog.getContentElement('informations', 'alt').setValue('');
    dialog.getContentElement('informations', 'border').setValue('');
    dialog.getContentElement('informations', 'hspace').setValue('');
    dialog.getContentElement('informations', 'vspace').setValue('');
    dialog.getContentElement('informations', 'float').setValue('Aucun');*/

    let element = dialog.getContentElement('informations', 'preview').getElement();
    let html = element.$.innerHTML;

    let previewsElement = document.createElement('div');
    previewsElement.innerHTML = html;

    let img = previewsElement.getElementsByTagName('img')[0];
    img.src = File.getUrl();
    img.classList.add('img-dge');
    img.style.width = File.width+'px';
    img.style.height = File.height+'px';

    element.setHtml(previewsElement.innerHTML);

    imceWin.close();
}

function changePreview(id) {
    let dialog = CKEDITOR.dialog.getCurrent();
    let element = dialog.getContentElement('informations', id).getElement();
    let value = dialog.getContentElement('informations', id).getValue();
    let preview = dialog.getContentElement('informations', 'preview').getElement();
    let html = preview.$.innerHTML;
    let previewsElement = document.createElement('div');

    previewsElement.innerHTML = html;
    let img = previewsElement.getElementsByTagName('img')[0];

    switch (id) {
        case "border":
            img.style.border = value+'px solid black';
            break;
        case "margin-top":
            img.style.marginTop = value+'px';
            break;
        case "margin-bottom":
            img.style.marginBottom = value+'px';
            break;
        case "margin-right":
            img.style.marginRight = value+'px';
            break;
        case "margin-left":
            img.style.marginLeft = value+'px';
            break;
        case "float":
            if (value === "Gauche") {
                img.style.cssFloat = 'left';
                img.classList.remove('img-float-right');
                img.classList.add('img-float-left');
            } else if (value === "Droite") {
                img.style.cssFloat = 'right';
                img.classList.remove('img-float-left');
                img.classList.add('img-float-right');
            } else if (value === "Aucun") {
                img.classList.remove('img-float-left');
                img.classList.remove('img-float-right');
                img.style.cssFloat = 'none';
            }
            break;
        case "width":
            let diffW = window.imageWidht/value;
            let height = Math.ceil(window.imageHeight/diffW);
            dialog.getContentElement('informations', 'height').setValue(height);
            img.style.height = height+'px';
            img.style.width = value+'px';
            break;
        case "height":
            let diffH = window.imageHeight/value;
            let width = Math.ceil(window.imageWidht/diffH);
            dialog.getContentElement('informations', 'width').setValue(width);
            img.style.width = width+'px';
            img.style.height = value+'px';
            break;
        case "alt":
            img.alt = value;
            break;
        case "title":
            img.title = value;
            break;
        default:
            break;
    }

    preview.setHtml(previewsElement.innerHTML);
}

function setPreview(check, div = null) {

    cleanHtmlDialog();

    if (check) {
        let elementType = div.tagName;
        let oldImg = div.getElementsByTagName('img')[0];
        let dialog = CKEDITOR.dialog.getCurrent();
        let element = dialog.getContentElement('informations', 'preview').getElement();
        let html = element.$.innerHTML;
        let previewsElement = document.createElement('div');
        previewsElement.innerHTML = html;
        let img = previewsElement.getElementsByTagName('img')[0];
        img.parentNode.replaceChild(oldImg, img);
        element.setHtml(previewsElement.innerHTML);

        let styles = oldImg.style;

        if (elementType === "A") {
            dialog.getContentElement('informations', 'lien').setValue(div.getAttribute("href"));
        }

        if (styles.width !== "") {
            dialog.getContentElement('informations', 'width').setValue(styles.width.replace(/\D/g,''));
        }

        if (styles.height !== "") {
            dialog.getContentElement('informations', 'height').setValue(styles.height.replace(/\D/g,''));
        }

        if (styles.border !== "") {
            dialog.getContentElement('informations', 'border').setValue(styles.border.replace(/\D/g,''));
        }

        if (styles.marginTop !== "") {
            dialog.getContentElement('informations', 'margin-top').setValue(styles.marginTop.replace(/\D/g,''));
        }

        if (styles.marginBottom !== "") {
            dialog.getContentElement('informations', 'margin-bottom').setValue(styles.marginBottom.replace(/\D/g,''));
        }

        if (styles.marginRight !== "") {
            dialog.getContentElement('informations', 'margin-right').setValue(styles.marginRight.replace(/\D/g,''));
        }

        if (styles.marginLeft !== "") {
            dialog.getContentElement('informations', 'margin-left').setValue(styles.marginLeft.replace(/\D/g,''));
        }

        if (oldImg.classList.contains('img-float-right')) {
            dialog.getContentElement('informations', 'float').setValue('Droite');
        }

        if (oldImg.classList.contains('img-float-left')) {
            dialog.getContentElement('informations', 'float').setValue('Gauche');
        }

        if (oldImg.title !== "") {
            dialog.getContentElement('informations', 'title').setValue(oldImg.title);
        }

        if (oldImg.alt !== "") {
            dialog.getContentElement('informations', 'alt').setValue(oldImg.alt);
        }

        let copyright = div.getElementsByClassName('copyright')[0];
        if (copyright !== undefined) {
            dialog.getContentElement('informations', 'copyright').setValue(copyright.textContent);
        }
        if (oldImg.src !== "") {
            setSizes(oldImg.src);
            dialog.getContentElement('informations', 'url').setValue(oldImg.src);
        }
    }
}

function cleanHtmlDialog() {
    let dialog = CKEDITOR.dialog.getCurrent();
    dialog.getContentElement('informations','preview').getElement().setHtml(
        '<div style="' +
        '   border: 2px ridge black;\n' +
        '   overflow: scroll;\n' +
        '   height: 400px;\n' +
        '   width: 600px;\n' +
        '   padding: 2px;\n' +
        '   background-color: white;"' +
        '   class="ImagePreviewBox">' +
        '   <table>' +
        '       <tbody>' +
        '           <tr>' +
        '               <td>' +
        '                   <img>' +
        '                   Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas feugiat consequat diam. Maecenas metus. Vivamus diam purus, cursus a, commodo non, facilisis vitae, nulla. ' +
        '                   Aenean dictum lacinia tortor. Nunc iaculis, nibh non iaculis aliquam, orci felis euismod neque, sed ornare massa mauris sed velit. Nulla pretium mi et risus. Fusce mi pede, tempor id, cursus ac, ullamcorper nec, enim. ' +
        '                   Sed tortor. Curabitur molestie. Duis velit augue, condimentum at, ultrices a, luctus ut, orci. Donec pellentesque egestas eros. Integer cursus, augue in cursus faucibus, eros pede bibendum sem, in tempus tellus justo quis ligula. ' +
        '                   Etiam eget tortor. Vestibulum rutrum, est ut placerat elementum, lectus nisl aliquam velit, tempor aliquam eros nunc nonummy metus. In eros metus, gravida a, gravida sed, lobortis id, turpis. Ut ultrices, ipsum at venenatis fringilla, sem nulla lacinia tellus, eget aliquet turpis mauris non enim. ' +
        '                   Nam turpis. Suspendisse lacinia. Curabitur ac tortor ut ipsum egestas elementum. Nunc imperdiet gravida mauris.' +
        '               </td>' +
        '           </tr>' +
        '       </tbody>' +
        '   </table>' +
        '</div>'
    );
}

function setSizes(url){
    var img = new Image();
    img.onload = function(){
        window.imageWidht = this.width;
        window.imageHeight = this.height;
    };
    img.src = url;
}