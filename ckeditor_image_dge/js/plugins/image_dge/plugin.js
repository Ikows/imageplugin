(function() {
    CKEDITOR.plugins.add('image_dge', {
        init: function(editor) {
            editor.config.allowedContent = true;
            editor.addCommand( 'image_dge', new CKEDITOR.dialogCommand( 'imageDGEDialog' ) );
            editor.ui.addButton('image_dge', {
                label: "Ajouter une image",
                command: 'image_dge',
                icon: this.path + 'icons/tooltip.png'
            });
            CKEDITOR.dialog.add( 'imageDGEDialog', this.path + 'dialogs/image_dge.js' );
            editor.on('afterInsertElement', function(event) {

            });
        }
    });
})();
